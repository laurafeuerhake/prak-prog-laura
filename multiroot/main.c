#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#define ALG gsl_multiroot_fsolver_hybrids //define the algorithm to the root solver

/* Exercise 1 - Rosenbrock */

int master_rosen(const gsl_vector * x, void *params, gsl_vector * f)
{
	// Rosenbrock: f(x0,x1) = (1-x0)² + 100(x1-x0²)² 
  const double x0 = gsl_vector_get (x, 0); 
  const double x1 = gsl_vector_get (x, 1);
  	// The gradient /Nabla f = (y0,y1)
  const double y0 = -2*(1-x0)-400*x0*(x1-pow(x0,2));
  const double y1 = 200*(x1-pow(x0,2));

  gsl_vector_set (f, 0, y0);
  gsl_vector_set (f, 1, y1)

  return GSL_SUCCESS;
}


/* Exercise 2 - the Hydrogen atom */
double Fe(double a, double r); // link the external function used

// master function
int master(const gsl_vector * x, void * params, gsl_vector * f){
	double e = gsl_vector_get(x,0); // energy
	double rmax = *(double*)params;
	gsl_vector_set(f,0,Fe(e,rmax));
	return GSL_SUCCESS;
}

int main(int argc, char *argv[]){
	const double rmax = atof(argv[1]);

// Exercise 1, Rosenbrock
	const int dim_rosen = 2;
	gsl_multiroot_function R;
	R.f = master_rosen;
	R.n = dim_rosen;
	R.params = NULL;

	gsl_multiroot_fsolver *s_rosen = gsl_multiroot_fsolver_alloc(ALG, dim_rosen);
	gsl_vector * x_rosen = gsl_vector_alloc(dim_rosen);
	gsl_vector_set(x_rosen,0,2);
	gsl_vector_set(x_rosen,1,2);
	gsl_multiroot_fsolver_set(s_rosen, &R, x_rosen);

	int iter_rosen=0,status_rosen;
	const double acc_rosen=1e-3;
	do{
		iter_rosen++;
		int flag_rosen=gsl_multiroot_fsolver_iterate(s_rosen);
		if(flag_rosen!=0)break;
		status_rosen = gsl_multiroot_test_residual (s_rosen->f, acc_rosen);

	}while(status_rosen == GSL_CONTINUE && iter_rosen<999); //max 99 iterations

	int iterr = iter_rosen;
	double x0r = gsl_vector_get(s_rosen->x,0);
	double x1r = gsl_vector_get(s_rosen->x,1);
	double f0r = gsl_vector_get(s_rosen->f,0);
	double f1r = gsl_vector_get(s_rosen->f,1);

	gsl_vector_free(x_rosen); //free memory allocation
	gsl_multiroot_fsolver_free (s_rosen);

	/* Exercise 2 - The Hydrogen atom */
	//
	const int dim = 1; //in the main function 1 dimension (1 function)
	//const double rmax = 8; // take a big r value, as infty not num. possible
	gsl_multiroot_function M; //make the multiroot function
	M.f = master;
	M.n = dim;
	M.params = (void*)&rmax; 

	gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc (ALG, dim); // allocate space in memory
	gsl_vector * x = gsl_vector_alloc(dim);
	gsl_vector_set(x,0,-.8);
	gsl_multiroot_fsolver_set (s, &M, x); 

	// the optimization:
	printf("# The following shows the different E and FE(rmax) values \n # for the iterations \n # iter, E,  Fe, \n");
	int iter=0,status;
	const double acc=1e-3;
	do{
		iter++;
		int flag=gsl_multiroot_fsolver_iterate(s);
		if(flag!=0)break;
		status = gsl_multiroot_test_residual (s->f, acc);
		/* print F and E for rmax for each iter in stderr */
		fprintf(stderr
			,"iter= %2i, E= %8f, Fe(rmax = %g)= %g\n"
			,iter
			,gsl_vector_get(s->x,0)
			,rmax
			,gsl_vector_get(s->f,0)
			);
		printf("%i %g %g \n",iter,gsl_vector_get(s->x,0),gsl_vector_get(s->f,0));
	}while(status == GSL_CONTINUE && iter<99); //max 99 iterations
	printf("\n\n");
	printf("%g %g %g \n",rmax,gsl_vector_get(s->x,0),gsl_vector_get(s->f,0));
	printf("\n\n");
	printf("# r , F(r) \n");
	double dr=0.1;
	for(double ri = 0.01; ri<10; ri+=dr){
		printf("%g %g\n", ri, Fe(gsl_vector_get(s->x,0),ri));
	}
	printf("\n\n");
	gsl_vector_free(x); //free memory allocation
	gsl_multiroot_fsolver_free (s);

	fprintf(stderr, "\n\n iter_rosen= %3i, x = %5f %5f, f(x) = %8g %8g \n \n\n",iterr,x0r,x1r,f0r,f1r);
return 0;
}
