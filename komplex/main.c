#include "komplex.h"
#include "complex.h"
#include "stdio.h"
#define TINY 1e-6

int main(){
		komplex x ={5,6};
		komplex y = {3,4};

		printf("testing komplex_add \n" );
		komplex add = komplex_add(x,y);
		komplex trueadd = {8,10};
		komplex_print("x= \n ",x);
		komplex_print("y = \n ",y);
		komplex_print("actual sum x+y =  \n", add);
		komplex_print("true sum x+y= \n", trueadd);

		printf("testing komplex_set \n" );
		komplex z = {x.re , y.im}; 
		komplex z2 = {5,6};
		komplex_set(&z2,5,4);
		komplex_print("actual z set \n ",z2);
		komplex_print("true z = \n ",z);
		if( komplex_equal(trueadd,add) )
				printf("test 'add' passed :) \n");
		else
				printf("test 'add' failed: debug me, please... \n");
return 0;
}

