
#include "stdio.h"
#include "nvector.h"
#include "stdlib.h"
#include <math.h>
#include <assert.h>
#define RND (double)rand()/RAND_MAX

//Allocates memory for at vector of a given size n
nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

//Frees the memory 
void nvector_free(nvector* v){ 
	free(v->data); free(v); v=NULL; 
}

void nvector_set(nvector* v, int i, double value){
	(*v).data[i]=value; 
}

//Gets the value which is stored at index i in nvector v.
double nvector_get(nvector* v, int i){
	return (*v).data[i]; 
}

//calculates the dot-product of two nvectors. Checks for equal size.
double nvector_dot_product (nvector* u, nvector* v){
  if ((*u).size == (*v).size) {
    double N = (*u).size;
    double sum = 0;
    int i;
    for (i = 0; i < N; i++) {
      sum = sum + (*u).data[i]*(*v).data[i];
    }
    return sum;
  }
  else {
    printf("Vectors are not the same size! ");
    return 0;
  }
}

void nvector_print    (char* s, nvector* v){    /* prints s and then vector */
   double N = (*v).size;
   int i = 0;
   printf("%s\n(",s);
   for (i = 0; i < N; i++) {
     if (i == N-1) {
       printf("%g",nvector_get(v,i));//Case where there should not be an komma at the end
     }
     else {
       printf("%g,",nvector_get(v,i));//Ordinary case
     }

   }
   printf(")\n");
}

//void nvector_set_zero (nvector* v);             /* all elements ← 0 */
int  nvector_equal    (nvector* u, nvector* v){
	int x = 1;
	int N = (*u).size;
	int c = -1;
  	while (x > 0 && c < N-1) {
  		c = c + 1;
    	if ((*u).data[c] == (*v).data[c]) x = 1;
    	else x = 0;
      fprintf(stderr, "x = %i, c = %i, N =%i \n", x,c,N);
    }
	if (c < N-1){
		return 0;
	}
	else{
		return 1;
	}
}
/* 1, if equal, 0 otherwise */
void nvector_add (nvector* u, nvector* v){  
  if ((*u).size == (*v).size) {
    int N = (*u).size;
    nvector* q = u;
    int i;
    for (i = 0; i < N; i++) {
      double s = nvector_get(v,i) + nvector_get(u,i);
      nvector_set(q,i,s);
    }
  }
  else {
    printf("Vectors are not the same size! ");
  }
}


//void nvector_sub      (nvector* a, nvector* b); /* a_i ← a_i - b_i */
//void nvector_scale    (nvector* a, double x);   /* a_i ← x*a_i     */

int main()
{	
	int n = 5;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");
	
  
	printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}	
  nvector_print(" a =",a);
  nvector_add(a,b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);

	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	nvector_free(v);
  
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);
  
	return 0;
}
