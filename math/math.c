#include "stdio.h"
#include "tgmath.h"
#include "math.h"
int  main(){
double x = 5;
double y = 0.5;
double tg=tgamma(x);
double b0 = j0(y);
double z=-2;
double complex s2 = csqrt(z);
double complex e1=exp(I);
double complex e2=exp(I*M_PI);
double complex e3 = pow(I,(exp(1)));
float xa = 0.1111111111111111111111111111;
double xb = 0.1111111111111111111111111111;
long double xc = 0.1111111111111111111111111111L;

printf("Math, Exercise 1 \n");
printf("gamma(5) = %g,\n bessel1 = %g\n",tg,b0);
printf("sqrt(-2) = %g+%gi\n",creal(s2),cimag(s2));
printf("exp(i) = %g+%gi\n",creal(e1),cimag(e1));
printf("exp(ipi) = %g+%gi\n",creal(e2),cimag(e2));
printf("i^e = %g+%gi\n",creal(e3),cimag(e3));
printf("float xa = %0.25g,\n double xb = %0.25lg,\n long double xc = %0.25Lg\n",xa,xb,xc);
printf("float stores 9 ones, double 16, long 22\n");
return 0;
} 
