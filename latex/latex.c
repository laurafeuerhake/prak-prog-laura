#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_equation
(double t, const double y[], double dydt[], void * params)
{
	dydt[0]= 2*pow(M_PI,-0.5)*exp(-pow(t,2));
return GSL_SUCCESS;
}

double system_function(double x){
	gsl_odeiv2_system sys;
	sys.function = diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[1]={0};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(int argc, char *argv[]){
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double dx = atof(argv[3]);
	for(double x=a;x<b+dx;x+=dx)printf("%g %g %g\n",x,system_function(x),sin(x));
return 0;
}
	