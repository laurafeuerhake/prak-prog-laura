#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_equation // set up differential equation, which can be solved by gsl
(double t, const double y[], double dydt[], void * params)
{
	dydt[0] = y[1];
	dydt[1] = - y[0];
	return GSL_SUCCESS;
}

double system_function(double x){
		

	gsl_odeiv2_system sys;
	sys.function = diff_equation; 
	sys.jacobian = NULL; 
	sys.dimension = 2; 
	sys.params = NULL; 

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new 
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[2]={1,0};
	gsl_odeiv2_driver_apply(driver,&t,x,y); 

	gsl_odeiv2_driver_free(driver); 
	return y[0]; 
}

double reduce_fct(double x){ // reduces argument to 0 < x <= 2*pi
	if(x<0){
		fprintf(stderr, "x_low=%g\n", x);
		return reduce_fct(-x);
	}
	if(x>2*M_PI){
		fprintf(stderr, "x_big=%g\n", x);
		return reduce_fct(x-2*M_PI);
	}
	fprintf(stderr, "x_final%g\n", x);
	return system_function(x);
}

int main(){
	double a = -3* M_PI; 
	double b = 3* M_PI; 
	double dx = 0.1;
	for(double x=a; x<b+dx; x+=dx)printf("%g %g %g\n",x,reduce_fct(x),cos(x));
	return 0;
}
