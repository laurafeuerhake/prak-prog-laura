#include "math.h"
#include "stdio.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_errno.h"
#include "gsl/gsl_multimin.h"
// #need to define ALG

double rosen(double x, double y){
	double r = pow((1-x),2) + 100*pow((y-pow(x,2)),2);
	return r;
}

double calc_rosen(const gsl_vector * v, void * params){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	return rosen(x,y);
}

int rosen_minimization(double xStart, double yStart){
	size_t dim = 2;
	gsl_multimin_function F;
	F.f = calc_rosen;
	F.n = dim;
	F.params = NULL;

	gsl_multimin_fminimizer * state = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,dim);
	gsl_vector *start = gsl_vector_alloc(dim);
	gsl_vector *step = gsl_vector_alloc(dim);
	gsl_vector_set(start,0,xStart); 
    gsl_vector_set(start,1,yStart);
    gsl_vector_set_all(step,0.05);
    gsl_multimin_fminimizer_set(state, &F, start, step);

  	int iteration = 0,status;
  	double accuracy = 0.001;
  	while(status == GSL_CONTINUE && iteration < 99) {
    	iteration++;
    	int flag = gsl_multimin_fminimizer_iterate(state);
    	if (flag!=0)break;
    	status = gsl_multimin_test_size (state->size,accuracy);
    	if (status == GSL_SUCCESS){
    		fprintf (stderr, "converged\n");
    	} 
    	fprintf(stderr,
	    "iter=%2i, x = %8f, y = %8f, rosenbrock = %g, size = %g\n",
	    iteration,
	    gsl_vector_get(state->x,0),
	    gsl_vector_get(state->x,1),
	    state->fval,
	    state->size);
  	}

  	fprintf(stderr, "\n\n");
  	gsl_vector_free(start);
  	gsl_vector_free(step);
  	gsl_multimin_fminimizer_free(state);
}


int main()
{
	double xStartRosen = 2;
	double yStartRosen = 2;
	rosen_minimization(xStartRosen, yStartRosen);
	return 0;
}