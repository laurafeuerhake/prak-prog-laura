#include "math.h"
#include "stdio.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_errno.h"
#include <gsl/gsl_multimin.h>
#include <assert.h>

struct data {int n; double *t, *y, *e;};

double object_function(const gsl_vector *x, double tk){
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	return A * exp(-tk/T) + B;
}

double least_square_function(const gsl_vector *x, void *params){
	struct data *p = (struct data*) params;
	int     n = p -> n;
	double *t = p -> t; // time
	double *y = p -> y; //activity
	double *e = p -> e; // exp. error
	double sum = 0;

	//double f = A * exp(-t/T) + B;
	double ft;
	for (int k = 0; k<n; k++){
		ft = object_function(x,t[k]);
		sum += pow((ft - y[k]) /e[k],2);
	}
	return sum;
}


int main(){
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);

	assert( n==sizeof(y)/sizeof(y[0]) && n==sizeof(e)/sizeof(e[0]));
	fprintf(stderr,"number of experimental points: %i\n",n);

	printf("# t[i], y[i], e[i]\n");
	for(int i=0;i<n;i++) {
		printf("%g %g %g\n",t[i],y[i],e[i]);
	}
	printf("\n\n");

	struct data params;
	params.n = n;
	params.t = t;
	params.y = y;
	params.e = e;

	size_t dim = 3;
	gsl_multimin_function F;
	F.f = least_square_function;
	F.n = dim;
	F.params = (void*)&params;

	gsl_vector *start = gsl_vector_alloc(F.n);
	gsl_vector_set(start,0,3);
	gsl_vector_set(start,1,2);
	gsl_vector_set(start,2,0);

	gsl_vector *step = gsl_vector_alloc(F.n);
	gsl_vector_set_all(step,2);

	gsl_multimin_fminimizer *state =
		gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,F.n);
	gsl_multimin_fminimizer_set (state, &F, start, step);

	int iteration = 0, status;
	double accuracy = 0.01;
	do{
		iteration++;
		int flag = gsl_multimin_fminimizer_iterate(state);
		if(flag != 0){
			fprintf(stderr,"unable to improve\n");
			break;
			}

		status = gsl_multimin_test_size(state->size, accuracy);
		if( status == GSL_SUCCESS ){
			fprintf(stderr,"converged\n");
		}
		fprintf(stderr,"iteration = %3i  ",iteration);
		fprintf(stderr,"initital value A = %.3f  ",gsl_vector_get(state->x,0));
		fprintf(stderr,"lifetime T = %.3f  ",gsl_vector_get(state->x,1));
		fprintf(stderr,"exp. error B = %.3f  ",gsl_vector_get(state->x,2));
		fprintf(stderr,"size = %.3f  ",state->size);
		fprintf(stderr,"fval/n= %.3f  ",state->fval/n);
		fprintf(stderr,"\n");

	}while( status == GSL_CONTINUE && iteration < 100);

	double AOpt = gsl_vector_get(state->x,0);
	double TOpt = gsl_vector_get(state->x,1);
	double BOpt = gsl_vector_get(state->x,2);

	printf("# theoretical, A*exp(-t/T)+B\n");
	double dt=(t[n-1]-t[0])/50;
	for(double ti = t[0]; ti<t[n-1]+dt; ti+=dt){
		printf("%g %g\n", ti, AOpt*exp(-ti/TOpt)+BOpt);
	}

gsl_multimin_fminimizer_free(state);
gsl_vector_free(start);
gsl_vector_free(step);
	return 0;
}
