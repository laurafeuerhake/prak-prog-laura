#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_equation
(double t, const double y[], double dydt[], void * params){
	double e = *(double *)params;
	dydt[0] = y[1];
//	fprintf(stderr,"diff %g",e);
	dydt[1] =  1 - y[0]+e*y[0]*y[0];
	return GSL_SUCCESS;
}

double system_function(double x, double e, double y0, double y1){
	gsl_odeiv2_system sys;
	sys.function = diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *) &e;
//fprintf(stderr,"system %g, %g",e,y1);
	double acc=1e-6,eps=1e-6,hstart=1e-3;
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);
	double t=0,y[2]={y0,y1};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}


int main(int argc, char *argv[]){
	double e = atof(argv[1]);
	double y1 = atof(argv[2]);
//	fprintf(stderr,"main %g, %g",e,y1);
	double a=0,b=33*M_PI,dx=0.02;
	for(double x=a;x<=b;x+=dx)printf("%g %g \n",x,system_function(x,e,1,y1));
		printf("\n\n");

	return 0;
}
