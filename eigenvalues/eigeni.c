#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_linalg.h>

// link the external function used
//gsl_vector solve_eq();
void matrix(gsl_vector y);

int main()
{
	gsl_vector *y = gsl_vector_alloc(3);
	gsl_vector_set_zero(y);
	matrix(*y);
	double x0 = gsl_vector_get(y,0);
	double x1 = gsl_vector_get(y,1);
	double x2 = gsl_vector_get(y,2);
	fprintf(stderr, "Solution \n x = (%g, %g, %g)\n", x0, x1, x2);
	printf("# solution to system of equations\n #x = (%g, %g, %g)\n", x0, x1, x2);
	printf("\n\n");

	/*------------------------*/
	/* eigenvalue - exercise */

	int n = 20;
	double s=1.0/(n+1);
	/*make Hamiltonian*/
	gsl_matrix *H = gsl_matrix_calloc(n,n); /*allocate matrix*/
	for(int i=0; i<n-1; i++){
		gsl_matrix_set(H,i,i,-2);  
  		gsl_matrix_set(H,i,i+1,1);
  		gsl_matrix_set(H,i+1,i,1);
	}
	gsl_matrix_set(H,n-1,n-1,-2); /*insert the last number of the diagonal*/
	gsl_matrix_scale(H,-1/s/s);
	/*diagonalize*/
	gsl_eigen_symmv_workspace* w =  gsl_eigen_symmv_alloc (n); /* tage hukommelse til beregning*/
	gsl_vector* eval = gsl_vector_alloc(n); 
	gsl_matrix* evec = gsl_matrix_calloc(n,n);
	gsl_eigen_symmv(H,eval,evec,w);  /* eigenvalues input is the hamiltonian, the place holders and the workspace - considered as a mathematical sub space*/
	/*sort eigenvectors and values*/
	gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC); /*sort eigenvalues with corresponding eigenvectors in ascending order (the eigenvalues)*/
	/* print out the n/2 smallest of the calculated eigenvalues compared to the theoretical of an infinite square well potential*/
	//printf("evec = %g\n\n",gsl_matrix_get(evec,0,0));

	fprintf (stderr, "i   exact   calculated\n"); /*specifies the output channel to stderr (default stdout)*/
	for (int k=0; k < n/2; k++){
	    double exact = M_PI*M_PI*(k+1)*(k+1); /*calculate the theoretical eigenvalues*/
	    double calculated = gsl_vector_get(eval,k); /*tag k'te indgang i egenværdi vektoren*/
	    fprintf (stderr, "%i   %g   %g  \n", k, exact, calculated);
	}
	/* plot the eigenfunctions */
	printf("# x values, energy eigenfunctions \n");
	for(int k=0;k<3;k++){
		printf("# %i. excited state \n",k);  
  		// prepare to have beautiful data
  		double a = 1;
  		if (gsl_matrix_get(evec,2,k) < 0){
 			a = -1;
	    }
	    // print the data
	    printf("%i %g\n",0,0.0);
  		for(int i=0;i<n;i++){
  			double vecelement = a* gsl_matrix_get(evec,i,k); //make data that has positive slope in the start
  			printf(" %g %g\n",(i+1.0)/(n+1),vecelement);
		}
		printf("%i %g\n",1,0.0);
		printf("\n\n");
  	}
  	printf("The accuracy for the energies is much better for a finer grid with bigger n.\n There the energies will be close to the analytical for more bound states than for less n");
	return 0;
}
