#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_linalg.h>

void matrix(gsl_vector* p){
	//Solve the equation A*x = b
	//create matrix A
	gsl_matrix *A = gsl_matrix_calloc(3,3);
	gsl_matrix_set(A,0,0,6.13);
	gsl_matrix_set(A,0,1,-2.9);
	gsl_matrix_set(A,0,2,5.86);
	gsl_matrix_set(A,1,0,8.08);
	gsl_matrix_set(A,1,1,-6.31);
	gsl_matrix_set(A,1,2,-3.89);
	gsl_matrix_set(A,2,0,-4.36);
	gsl_matrix_set(A,2,1,1);
	gsl_matrix_set(A,2,2,0.19);
	//create vector b
	gsl_vector *b = gsl_vector_alloc(3);
	gsl_vector_set(b,0,6.23);
	gsl_vector_set(b,1,5.37);
	gsl_vector_set(b,2,2.29);
	//define solution vector x
	gsl_vector *x = gsl_vector_alloc(3);
	// solve the liner equation
	gsl_linalg_HH_solve(A,b,x);
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double x2 = gsl_vector_get(x,2);
	fprintf(stderr, "Solution \n x = (%g, %g, %g)\n", x0, x1, x2);
	gsl_matrix_free(A);
	gsl_vector_set(p,0,x0);
	gsl_vector_set(p,1,x1);
	gsl_vector_set(p,2,x2);

}
