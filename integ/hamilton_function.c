#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>


double hamilton_integrand(double x, void* params){
	double alpha = *(double *)params;
	return (-pow(alpha,2)*pow(x,2)/2 + alpha/2 + pow(x,2)/2)*exp(-alpha*pow(x,2));
}

double hamilton_function(double alpha){
	gsl_function f;
	f.function = hamilton_integrand;
	f.params = (void *) &alpha;

	int limit = 100;
	double acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
int status=gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}