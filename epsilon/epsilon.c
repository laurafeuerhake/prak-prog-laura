#include "limits.h"
#include "float.h"
#include "math.h"
#include "stdio.h"
int n = INT_MAX;

int main()
{
	int i=1; 
	while(i+1>i){
		i++;
	}
	int j = 1;
	do{
		j++;
	} while (j+1>j);
	int k = 1;
	for (int h = 1; h < h+1; h++){
		k++;
	}
	printf("limits max int = %i\n",n);
	printf("limits max int, while loop = %i\n",i);
	printf("limits max int, do-while loop = %i\n",j);
	printf("limits max int, for loop = %i\n",k);
	return 0;
}