#include "limits.h"
#include "float.h"
#include "math.h"
#include "stdio.h"

double xlimit = DBL_EPSILON;
float ylimit = FLT_EPSILON;
double long zlimit = LDBL_EPSILON;

int main(){
	/*While loop*/
	double xi=1;
	while(1+xi!=1){
		xi/=2;
	}
	xi*=2;
	float yi=1;
	while(1+yi!=1){
		yi/=2;
	}
	yi*=2;
	double long zi=1;
	while(1+zi!=1){
		zi/=2;
	}
	zi*=2;
	/*do-while loop*/
	double xj=1;
	do{
		xj/=2;
	}
	while(1+xj!=1);
	xj*=2;
	float yj=1;
	do{
		yj/=2;
	}
	while(1+yj!=1);
	yj*=2;
	double long zj=1;
	do{
		zj/=2;
	}
	while(1+zj!=1);
	zj*=2;
	/*for loop*/
	double xk=1;
	for(xk=1; 1+xk!=1; xk/=2){;} 
	xk*=2;
	float yk=1;
	for(yk=1; 1+yk!=1; yk/=2){;} 
	yk*=2;
	long double zk=1;
	for (zk=1; 1+zk!=1;zk/=2){;}
	zk*=2;

	printf("while double epsilon = %lg,\n do-while double epsilon = %lg,\n for double epsilon = %lg,\n limits double epsilon = %lg\n",xi,xj,xk,xlimit);
	printf("while float epsilon = %g,\n do-while float epsilon = %g,\n for float epsilon = %g,\n limits float epsilon = %g\n",yi,yj,yk,ylimit);
	printf("while long double epsilon = %Lg,\n do-while long double epsilon = %Lg,\n for long double epsilon = %Lg,\n limits long double epsilon = %Lg\n",zi,zj,zk,zlimit);
return 0;
}
