#include "limits.h"
#include "float.h"
#include "math.h"
#include "stdio.h"

int main(){
	int res = equal(1, 1, 1, 1);
	printf("exercise 3: result = %i\n", res);
}



int equal(double a, double b, double tau, double eps){
	if(fabs(a-b)<tau || fabs(a-b)/(fabs(a)+fabs(b)) < eps/2){
		return 1;
	}
	else{
		return 0;
	}
}
