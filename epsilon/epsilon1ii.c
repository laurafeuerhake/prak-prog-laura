#include "limits.h"
#include "float.h"
#include "math.h"
#include "stdio.h"
int n = INT_MIN;

int main()
{
	int i=0; 
	while(i-1<i){
		i--;
	}
	int j = 0;
	do{
		j--;
	} while (j-1<j);
	int k = 0;
	for (int h = 0; h > h-1; h--){
		k++;
	}
	printf("limits min int = %i\n",n);
	printf("limits min int, while loop = %i\n",i);
	printf("limits min int, do-while loop = %i\n",j);
	printf("limits min int, for loop = %i\n",k);
	return 0;
}