#include "limits.h"
#include "float.h"
#include "math.h"
#include "stdio.h"

int max=INT_MAX/3;
int SumUpFloat();
int SumDownFloat();

int main(){
	SumUpFloat();
	SumDownFloat();
	return 0;
}

int SumUpFloat(){
	float sum_up_float;
	int i=1;
	while(i<max+1){
		sum_up_float = sum_up_float+1.0f/i;
		i++;
	}
	printf(" sum up = %g\n", sum_up_float);
	return 0;
}

int SumDownFloat(){
	float sum_down_float;
	int i=max;
	while(i>0){
		sum_down_float = sum_down_float+1.0f/i;
		i--;
	}
	printf(" sum down = %g\n", sum_down_float);
	return 0;
}
